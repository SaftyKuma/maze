package cell

import (
    "testing"
)

// Test000 tests that a cell gets created properly.
func Test000 (t *testing.T) {
    
    got := New()
    if got.Visited {
        t.Errorf("{Visited} got true; want false")
    }
    
    if !got.Walls[0] || !got.Walls[1] || !got.Walls[2] || !got.Walls[3] {
        t.Errorf("{Walls} got false; want true")
    }
}