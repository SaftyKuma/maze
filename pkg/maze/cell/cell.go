// Package cell implements a type for the cells within a maze.
package cell

// Cell represents a "Cell" inside a maze.
type Cell struct {
    // Visited specifies if the cell has been marked as "visited"
    Visited bool
    // Walls mark if a wall exists in a specific direction. 
    // If the value is true, the wall is considered to exist.
    Walls [4]bool
}

// New creates a pointer to a new Cell.
// The created cell is considered to not have been visited, and has walls in all four directions.
func New() (cellPtr *Cell) {
    cellPtr = new(Cell)
    cellPtr.Visited = false
    cellPtr.Walls = [4]bool{true, true, true, true}
    return
}

// WallCount counts the number of walls in the cell and returns the result as an int.
func WallCount(c Cell) (count int) {
    count = 0
    for _, side := range c.Walls {
        if side {
            count++
        }
    }
    return
}