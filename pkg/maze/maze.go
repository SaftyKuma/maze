// Package maze implements a simple maze generator.
package maze

import "fmt"
import "errors"

import "gitlab.com/SaftyKuma/maze/pkg/maze/cell"
import "gitlab.com/SaftyKuma/maze/pkg/maze/randsort"

// Constants for the four cardinal directions.
const (
    North = 0
    South = 1
    East = 2
    West = 3
)

// Opposite returns an int representing the opposite direction for the given direction.
// This function will panic if an int outside the range 0-3 is specified.
func Opposite(direction int) int {

    switch direction {
    case North:
        return South
    case South:
        return North
    case East:
        return West
    case West:
        return East
    default:
        panic(fmt.Sprintf("Got %d; expeted 0,1,2, or 3", direction))
    }
}


// Grid represents the maze and its containing cells.
type Grid struct {
    // W represents the width in cells.
    // H represents the height in cells.
    W, H int
    // Cells contains a pointer to each cell in the grid.
    Cells [][]*cell.Cell
}

// New returns a pointer to a new Grid with width w and height w, with no paths created.
// This function will return an error if the values for w and h are not valid (less than 1).
func New(w, h int) (*Grid, error) {
    
    if w < 1 || h < 1 {
        return nil, errors.New("Invalid dimensions specified.")
    }
    
    gridPtr := new(Grid)
    gridPtr.W = w
    gridPtr.H = h
    gridPtr.Cells = make([][]*cell.Cell, w)
    for i := 0; i < w; i++ {
        gridPtr.Cells[i] = make([]*cell.Cell, h)
        for j :=0; j < h; j++ {
            gridPtr.Cells[i][j] = cell.New()
        }
    }
    return gridPtr, nil
}

// InGrid returns true if the provided x and y coordinates are considered to be inside the grid's bounds.
func (g Grid) InGrid(x, y int) bool {
    
    if x < 0 || x > g.W - 1 {
        return false
    } 

    if y < 0 || y > g.H - 1 {
        return false
    }

    return true
}

// ValidForPath returns true if the provided x and y coordinates are valid to be traversed. 
// This means they are both inside grid and the cell at those coordinates has yet to be visited.
func (g Grid) ValidForPath(x, y int) bool {
    
    if !g.InGrid(x, y) {
        return false
    }

    if g.Cells[x][y].Visited {
        return false
    }

    return true
}

// Path starts pathing at coordinates x and y. 
// This function is called recursively.
func (g *Grid) Path(x, y int){

    // Mark cell at x,y as visited
    g.Cells[x][y].Visited = true

    // Randomly try directions
    dirs := randsort.SortInt([]int{North, South, East, West})
    for _, el := range dirs {
        var dx, dy int;
        switch el {
        case North:
            dx = 0
            dy = -1
        case South:
            dx = 0
            dy = 1
        case East:
            dx = 1
            dy = 0
        case West:
            dx = -1
            dy = 0
        }
        nX, nY := x + dx, y + dy
        if g.ValidForPath(nX, nY) {
            // If the next coord is valid, remove walls and continue pathing.
            g.Cells[x][y].Walls[el] = false
            g.Cells[nX][nY].Walls[Opposite(el)] = false
            g.Path(nX, nY)
        }
    }
    // We are finished with this cell if there are no neighbors left that are unvisited.
    return
}