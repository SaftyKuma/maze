// Package randsort provides a means to randomly sort/reorder slices.
package randsort

import "math/rand"

// SortInt returns a new slice with all of the elements in s, reordered at random.
// This function should not modify the original slice.
func SortInt(s []int) (retslice []int) {
    sc := make([]int,len(s))
    copy(sc, s)
    retslice = make([]int, 0)
    
    for len(sc) > 0 {
        r := rand.Intn(len(sc))
        retslice = append(retslice, sc[r])
        sc = append(sc[:r], sc[r + 1:]...)
    }

    return
}