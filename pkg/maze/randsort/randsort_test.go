package randsort

import (
    "testing"
    "time"
    "math/rand"
)

//Test000 tests against slice: int[]{1, 2, 3, 4}
func Test000 (t *testing.T) {
    rand.Seed(time.Now().Unix())
    in := []int{1, 2, 3, 4}
    got := SortInt(in)
    t.Log("in =", in)
    t.Log("got =", got)
    if len(in) != len(got) {
        t.Errorf("%d != %d; want len(in) == len(out)", len(in), len(got))
    }
}

//Test001 tests against slice: int[]{}
func Test001 (t *testing.T) {
    rand.Seed(time.Now().Unix())
    in := []int{}
    got := SortInt(in)
    t.Log("in =", in)
    t.Log("got =", got)
    if len(got) != 0 {
        t.Errorf("got %d; want 0", len(got))
    }
}

//Test002 tests against slice: int[]{2}
func Test002 (t *testing.T) {
    rand.Seed(time.Now().Unix())
    in := []int{2}
    got := SortInt(in)
    t.Log("in =", in)
    t.Log("got =", got)
    if len(in) != 1 {
        t.Errorf("got %d; want 1", len(got))
    }
}

//Test003 tests against slice: int[]{-8, 6, -7, -5, 3, 0, 9}
func Test003 (t *testing.T) {
    rand.Seed(time.Now().Unix())
    in := []int{-8, 6, -7, -5, 3, 0, 9}
    got := SortInt(in)
    t.Log("in =", in)
    t.Log("got =", got)
    if len(in) != len(got){
        t.Errorf("%d != %d; want len(in) == len(out)", len(in), len(got))
    }
}