# maze

A Go package which implements a simple maze generator.

## License
[MIT](https://chosealicense.com/licenses/mit/)