// Package main implements a utility which generates an svg maze.
package main

import (
    "math/rand"
    "log"
    "time"
    "os"
    "flag"
    "fmt"

    "gitlab.com/SaftyKuma/maze/pkg/maze"
    "github.com/ajstarks/svgo"
)

var (
    // The random seed.
    randSeed int64 
    // The size of the maze in cells.
    mazeWidth, mazeHeight int
    // The output file for the SVG.
    outFile string
)

// init processes command line args.
func init() {
    flag.Usage = func() {
        fmt.Printf("%s: Generates an SVG image of a maze.\nUsage:\n", os.Args[0])
        flag.PrintDefaults()
    }
    flag.Int64Var(&randSeed, "s", 0, "Specifies a random seed. If 0 is specified, the current Unix time is used as the seed.")
    flag.IntVar(&mazeWidth, "w", 10, "Specifies the height of the maze in cells.")
    flag.IntVar(&mazeHeight, "h", 10, "Specifies the height of the maze in cells.")
    flag.StringVar(&outFile, "o", "", "Specifies an output file to write the SVG data. If no file is specified, output will be sent to STDOUT.")
    flag.Parse()
    if randSeed == 0 {
        randSeed = time.Now().Unix()
    }
}


// main function
func main() {
    
    rand.Seed(randSeed)
    
    // Generate the maze
    maze, err := maze.New(mazeWidth, mazeHeight)
    if err != nil {
        log.Fatalln(err)
    }
    maze.Path(0, 0)

    // Create the SVG
    wScale := 10    // Scale for drawing purposes.
    w := mazeWidth * wScale
    h := mazeHeight * wScale
    var canvas *svg.SVG
    if (outFile == "") {
        canvas = svg.New(os.Stdout)
    } else {
        iow, err := os.Create(outFile)
        if err != nil {
            log.Fatalln(err)
        }
        
        canvas = svg.New(iow)
    }

    canvas.Start(w, h)

    for i, _ := range maze.Cells {
        for j, elj := range maze.Cells[i] {
            if elj.Walls[0] {
                canvas.Line(i * wScale, j * wScale, (i + 1) * wScale, j * wScale, "stroke:black; stroke-width: 2")
            }
            if elj.Walls[1] {
                canvas.Line(i * wScale, (j + 1) * wScale, (i + 1) * wScale, (j + 1) * wScale, "stroke:black; stroke-width: 2")
            }
            if elj.Walls[2] {
                canvas.Line((i + 1) * wScale, j * wScale, (i+1) * wScale, (j + 1) * wScale, "stroke:black; stroke-width: 2")
            }
            if elj.Walls[3] {
                canvas.Line(i * wScale, j * wScale, i* wScale, (j + 1) * wScale, "stroke:black; stroke-width: 2")
            }
        }
    }

    canvas.End()
}